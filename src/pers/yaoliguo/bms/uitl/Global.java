package pers.yaoliguo.bms.uitl;

/**
 * @ClassName:       Global
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月24日        下午3:21:37
 */
public class Global {

	 public static final String LOGIN_USER_KEY = "loginUser";
	 
	 public static final String LOGIN_ROLE_KEY = "loginRole";
	
}
