package pers.yaoliguo.bms.uitl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @ClassName:       StringHelper
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月5日        下午8:10:09
 */
public class StringHelper
{

	/**
	 * 生成32位UUID
	 * 
	 * @return string
	 */
	public static String getUUID()
	{
		String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
		return uuid;
	}

	/**
	 * 用当前时间+N位随机数
	 * 
	 * @return
	 */
	public static String generateFileName()
	{
		// 获得当前时间
		DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		// 转换为字符串
		String formatDate = format.format(new Date());
		// 随机生成文件编号
		int random = new Random().nextInt(10000);
		return new StringBuffer().append(formatDate).append(random).toString();
	}

	/**
	 * 判断字符串是否为空
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNullOrEmpty(String str)
	{
		return str == null || str.equals("");
	}

	// 是否电话号码
	public static boolean isMobileNO(String mobiles)
	{
		if (StringHelper.isNullOrEmpty(mobiles))
		{
			return false;
		}
		Pattern p = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");

		Matcher m = p.matcher(mobiles);

		return m.matches();

	}
	
}

