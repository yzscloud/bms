package pers.yaoliguo.bms.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SysRoleMenuKey implements Serializable{
    private String menuId;

    private String roleId;
    
    private List<SysMenu>children = new ArrayList<SysMenu>();
    
    public List<SysMenu> getChildren() {
		return children;
	}

	public void setChildren(List<SysMenu> children) {
		this.children = children;
	}

	public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId == null ? null : menuId.trim();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }
}