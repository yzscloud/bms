package pers.yaoliguo.bms.control.view;

import java.util.List;

import pers.yaoliguo.bms.common.PagerBean;

/**
 * @ClassName:       Message
 * @Description:    统一的消息模型
 * @author:            yao
 * @date:            2017年7月1日        下午3:27:21
 */
public class Message <T>{
	/**
	 * 返回结果编码
	 * 200  请求成功
	 * 400 请求有误
	 * 500 服务器出现异常
	 */
	private String result;
	
	/**
	 * 返回请求操作信息： 正常可以不需要设置，异常需要设置，便于定位
	 */
	private String info;
	
	private T data;
	
	private List<T> dataList;
	/**
	 * 添加分页信息
	 */
	private PagerBean page;
	
	public PagerBean getPage() {
		return page;
	}

	public void setPage(PagerBean page) {
		this.page = page;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<T> getDataList() {
		return dataList;
	}

	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}

}
