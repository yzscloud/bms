package bms.junit;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.activiti.image.ProcessDiagramGenerator;
import org.activiti.image.impl.DefaultProcessDiagramGenerator;

/**
 * @ClassName:       ProcessTest
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年8月20日        下午3:47:52
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring_*.xml"})  
public class ProcessTest {
	
	@Autowired
	ProcessEngine engine;
	
	@Autowired
    ProcessEngineConfiguration processEngineConfiguration;
	
	@Test
	public void testDepylist(){
		List<Deployment> list = engine.getRepositoryService().createDeploymentQuery().list();
		for (Deployment deployment : list) {
			System.out.println("部署名："+deployment.getName());
			System.out.println("部署id："+deployment.getId());
		}
	}
	
	@Test
	public void testStartProocess(){
		Map<String,Object> vue = new HashMap<String,Object>();
		vue.put("inputUser", "张三");
		String objid = "27541.2d21707c2e0e4bcc82cb9bdcf0e67ec1";
		String depId = "27541";
		ProcessDefinition Pdf =  engine.getRepositoryService()//
				.createNativeProcessDefinitionQuery()//
				.sql("SELECT * FROM "+engine.getManagementService().getTableName((ProcessDefinition.class))+" WHERE DEPLOYMENT_ID_="+depId)//
				.singleResult();
		
		//engine.getRuntimeService().startProcessInstanceById(Pdf.getId(), objid, vue);
		engine.getRuntimeService().startProcessInstanceByKey("myLeaveBillprocess",  objid, vue);
	}
	
	@Test
	public void getTask(){
		List<Task> list = engine.getTaskService().createTaskQuery().taskAssignee("张三").list();
		for(Task task : list){
    		System.out.println("任务id："+task.getId());
    		System.out.println("任务名称："+task.getName());
    		System.out.println("受理人："+task.getAssignee());
    		System.out.println("执行id："+task.getExecutionId());
    	}
	}
	
	@Test
	public void commitTask(){
		String tId = "32504";
		TaskService ts = engine.getTaskService();
		Task tk = ts.createTaskQuery().taskId(tId).singleResult();
		//根据任务id得到流程实例id
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("outcome", "驳回");
		map.put("inputUser", "张三");
		String message = "不批准这次请假";
		String processInstanceId = tk.getProcessInstanceId();//添加批注
		ts.addComment(tId, processInstanceId, message);
		ts.complete(tId, map);
		
	}
	
	//查询活动后的出口，就是连线上的字  驳回  批准
	@Test
	public void findOutgoingTransitions(){
		String tId = "35003";
		
		Task task = engine.getTaskService().createTaskQuery().taskId(tId).singleResult();
		//获取流程定义ID
		String processDefinitionId = task.getProcessDefinitionId();
		
		ProcessDefinitionEntity pd = (ProcessDefinitionEntity) engine.getRepositoryService()//
																		.getProcessDefinition(processDefinitionId);
		
		//使用流程实例ID，查询正在执行的执行对象表，返回流程实例对象
		String InstanceId = task.getProcessInstanceId();
		
		ProcessInstance pi = engine.getRuntimeService()//
										.createProcessInstanceQuery()//
										.processInstanceId(InstanceId)//
										.singleResult();
		//获取当前的活动
		ActivityImpl activityImpl = pd.findActivity(pi.getActivityId());
		
		//获取当前活动完成之后连线的名称
		List<PvmTransition> pvmList = activityImpl.getOutgoingTransitions();
		if(pvmList!=null && pvmList.size()>0){
			for(PvmTransition pvm:pvmList){
				String name = (String) pvm.getProperty("name");
				System.out.println("出口的名字:"+name);
			}
		}
		
		
	}
	
	@Test
	public void viewImg(){
		
		String tId = "35003";
		
		Task task = engine.getTaskService().createTaskQuery().taskId(tId).singleResult();
		//获取流程定义ID
		String processDefinitionId = task.getProcessDefinitionId();
		
		ProcessDefinitionEntity pde = (ProcessDefinitionEntity) engine.getRepositoryService()//
																		.getProcessDefinition(processDefinitionId);
		
		//使用流程实例ID，查询正在执行的执行对象表，返回流程实例对象
		String InstanceId = task.getProcessInstanceId();
		
		ProcessInstance pi = engine.getRuntimeService()//
										.createProcessInstanceQuery()//
										.processInstanceId(InstanceId)//
										.singleResult();
		List<Execution> executions = engine.getRuntimeService()//
								.createExecutionQuery()//
								.processInstanceId(InstanceId)//
								.list();
		RuntimeService runtimeService = engine.getRuntimeService();
		//得到正在执行的Activity的Id
        List<String> activityIds = new ArrayList<String>();
        for (Execution exe : executions) {
            List<String> ids = runtimeService.getActiveActivityIds(exe.getId());
            activityIds.addAll(ids);
        }
     
       //获取流程图
       BpmnModel bpmnModel = engine.getRepositoryService().getBpmnModel(pi.getProcessDefinitionId());
        
       ProcessDiagramGenerator diagramGenerator = processEngineConfiguration.getProcessDiagramGenerator();
       InputStream in = diagramGenerator.generateDiagram(bpmnModel,"png",activityIds);
        
        FileOutputStream out = null;
        try {
			 out = new FileOutputStream("F:\\user\\bpmn\\1.png");
			 byte [] buf = new byte[1024];
			 int lenth = 0;
			 while ((lenth = in.read()) != -1) {
					out.write(buf, 0, lenth);
			 }
			 out.flush();
			 } catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(out != null){
				try {
					in.close();
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
	}

}
