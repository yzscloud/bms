var Helper = {
	IsEmpty: function (v, allowBlank)
    {
        return v === null || v === undefined || (!allowBlank ? v === "" : false)
    },
    getInt: function (v)
    {
        var r = 0;
        if (!Helper.IsEmpty(v)) { try { r = parseInt(v); } catch (e) { r = 0; } }
        return r;
    },
    isDate: function (v)
    {
        return v && typeof v.getFullYear == "function"
    }
}
Helper.Url = new (function ()
{
    var _allArgs = null;
    this.GetArgs = function ()
    {
        var args = new Object(); //声明一个空对象 
	    var query = window.location.search.substring(1); // 取查询字符串，
	    var pairs = query.split("&"); // 以 & 符分开成数组 
	    for (var i = 0; i < pairs.length; i++)
	    {
	        var pos = pairs[i].indexOf('='); // 查找 "name=value" 对 
	        if (pos == -1) continue; // 若不成对，则跳出循环继续下一对 
	        var argname = pairs[i].substring(0, pos); // 取参数名 
	        var value = pairs[i].substring(pos + 1); // 取参数值 
	        value = decodeURIComponent(value); // 若需要，则解码 
	        args[argname] = value; // 存成对象的一个属性 
	    }
	    return args; // 返回此对象 
    };
	this.QueryString = function (name)
	{
	    if (_allArgs == null) _allArgs = this.GetArgs();
	    if (Helper.IsEmpty(name)) return _allArgs; //不带参数,则返回所有
	    else
	    {
	        for (var i in _allArgs)
	            if (i.toLowerCase() == name.toLowerCase())
	                return _allArgs[i];
	        return ''; //默认返回空字串
	    }
	};

	this.getDomain = function()
	{
		var portStr = "";
		if(!Helper.IsEmpty(window.location.port))
		{
			portStr = ":"+window.location.port;
		}
		return window.location.protocol+"//"+window.location.hostname+portStr;
    }

})();


var App = {
		basePath:Helper.Url.getDomain()+"/bms"
};


